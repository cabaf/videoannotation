import json
import os
import random

"""
*****
IMPORTANT JSON STRUCTURE:

{"Quality": "", "Yachting": [[19.09377426, 50.22048966], [56.85834286, 79.34747326]]}

QUALITY DETERMINES QUALITY OF ACTIVITY IN ANNOTATIONS

QUALITY VALUES: "good","decent" or "bad"

*****
"""

ANNOTATION_PATH = "../data/annotations.mturk"
RESULTS_PATH = "../data/annotations.quality"
STATUS_JSON = "../data/video_status.json"

def return_template(videoid, activity, start, end, annotation_id):
    video_info = {"id": "SXOSzD_iQtq6MpYmzvtr_A",
                  "media": "video",
                  "annotator_schema_version": "v1.0",
                  "created": "2015-05-24T18:52:08.036814",
                  "updated": "2015-05-26T12:17:05.012544",
                  "text": activity,
                  "quote": "",
                  "uri": "http://example.com",
                  "ranges": [],
                  "user": "mturk",
                  "consumer": "mturk",
                  "annotation_id": annotation_id,
                  "rangeTime": {
                    "start": start,
                    "end": end
                  },
                  "target": {
                    "container": "video-player",
                    "src": "http://www.youtube.com/watch?v=%s" % videoid
                  },
                  "permissions": {
                    "read": [],
                    "admin": [],
                    "update": [],
                    "delete": []
                  }
                 }
    return video_info

def dump_quality_results(videoid, userid, results, path=RESULTS_PATH):
    
    filename = os.path.join(path, "%s.json" % videoid)
    
     
    with open(filename, "r") as fobj:
        json_video = json.load(fobj)
             
    """
    I think results should have the following syntaxis:
    
    
    {"0000011": {"start": 7.00033962, "end": 17.24987251000001, "quality": "bad", "label": "Polishing shoes"}, 
    "0000012": {"start": 74.55480414, "end": 104.92298380000001, "quality": "good", "label": "Polishing shoes"}}
    
    So, it means results is value of annotation's key
    
    NOTE: results is a json structure.
    
    """
    
    json_video['annotations']=results   
    
    with open(filename, "w") as fobjs:
        fobjs.write(json.dumps(json_video))
        
    """
    JSON OUTPUT STRUCTURE:
    
    {"video_id": "EcQ7DcVefdw", "truepositive": True, "related": True, "annotations": {"0000011": {"start": 8.00033962, "end": 57.24987251000001, "quality":"fix", "label": "Polishing shoes"}, 
    "0000012": {"start": 72.55480414, "end": 104.92298380000001, "quality": "good", "label": "Polishing shoes"}}}
    """
    update_video_state(videoid, userid, "Evaluated")
    return True

def update_video_state(videoid, userid, state, filename=STATUS_JSON):
    with open(filename, "r") as fobj:
        video_status = json.load(fobj)
    video_status[videoid]["status"] = state
    video_status[videoid]["userid"] = userid
    with open(filename, "w") as fobj:
        fobj.write(json.dumps(video_status))

def retrieve_video(userid, filename=STATUS_JSON):
    available = []
    with open(filename, "r") as fobj:
        video_status = json.load(fobj)
    for vid in video_status:
        if video_status[vid]["status"] == "uncompleted":
            available.append(vid)
    videoid = random.choice(available)
    update_video_state(videoid, userid, "busy")
    return videoid

def get_video_info(videoid, path=ANNOTATION_PATH):
    json_file = os.path.join(path, "%s.json" % videoid)
    with open(json_file, "r") as fobj:
        video_data = json.load(fobj)
    video_annotations = []
    video_data=video_data['annotations']
    for ann_i in sorted (video_data.keys()):
        start=video_data[ann_i]['start']
        end=video_data[ann_i]['end']
        activity=video_data[ann_i]['label']
        video_annotations.append(return_template(videoid, activity, start, end))
    return video_annotations
    
def dump_related_truepositive (videoid,truepositive,related,path_open=ANNOTATION_PATH,path_write=RESULTS_PATH):
    file_open=os.path.join(path_open, "%s.json" % videoid)
    filename=os.path.join(path_write, "%s.json" % videoid)
    with open(file_open, "r") as fobj:
        json_data = json.load(fobj)
    json_data['truepositive']=truepositive
    json_data['related']=related
    with open(filename, "w") as fobj:
        fobj.write(json.dumps(json_data)) 
    return 
        
def verif_trueposivite_related (videoid,path=RESULTS_PATH):
    file_open=os.path.join(path,"%s.json" % videoid)
    with open(file_open,"r") as fobj:
        json_data=json.load(fobj)
    #Validate true values
    if json_data['truepositive'] and json_data['related']:
        done=True
    else:
        done=False
    
    return done