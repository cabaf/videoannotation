#!/usr/bin/python
import cgi, json

from utils import get_video_info
from utils import dump_quality_results
from utils import retrieve_video
from utils import update_video_state
from utils import dump_related_truepositive
from utils import verif_trueposivite_related

def retrieve_video_info(form):
    videoid = form.getvalue('videoid')
    video_info = get_video_info(videoid)
    print 'Content-Type: application/json\n\n'
    print json.dumps(video_info)

def save_quality_results(form):
    videoid = form.getvalue('videoid')
    userid = form.getvalue('userid')
    results = form.getvalue('results')
    status = dump_quality_results(videoid, userid, results)
    print 'Content-Type: application/json\n\n'
    print json.dumps(status)

def get_random_video(form):
    userid = form.getvalue('userid')
    videoid = retrieve_video(userid)
    print 'Content-Type: application/json\n\n'
    print json.dumps(videoid)

def enable_video(form):
    videoid = form.getvalue('videoid')
    update_video_state(videoid, None, "uncompleted")
    print 'Content-Type: application/json\n\n'
    print json.dumps("True")

def save_truepositive_related (form):
    videoid = form.getvalue('videoid')
    truepositive=form.getvalue('truepositive')
    related=form.getvalue('related')
    dump_related_truepositive (videoid,truepositive,related)
    
def validated_truepositive_related (form):
    videoid = form.getvalue('videoid')
    state=verif_trueposivite_related(videoid)
    print 'Content-Type: application/json\n\n'
    print json.dumps(state)
    

if __name__ == '__main__':
    form = cgi.FieldStorage()
    action = form.getvalue('action')
    if action == 'retrieve_video_info':
        retrieve_video_info(form)
    elif action == 'save_quality_results':
        save_quality_results(form)
    elif action == 'get_random_video':
        get_random_video(form)
   
    elif action == 'save_truepositive_related':
        save_truepositive_related(form)
      
    elif action == 'enable_video':
        enable_video(form)
    
    elif action == 'validated_truepositive_related':
        validated_truepositive_related(form)
        
    else:
        print 'Content-Type: application/json\n\n'
        print json.dumps('ERROR')
