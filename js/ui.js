function boot_ui() {
    var base_html = "<div id=\"airlock\">" +
        "<video id=\"video-player\" class=\"video-js vjs-default-skin\" controls preload=\"none\" width=\"645\" height=\"365\"" +
        "data-setup='{techOrder: [\"html5\",\"flash\",\"youtube\"]}'>" +
            "<source src=\"http://www.youtube.com/watch?v=%s\" type=\"video/youtube\">" +
        "</video>" +
    "</div>"
    html_ = sprintf(base_html, params.videoid);
    $( html_ ).appendTo( "#video-bar" );
    setup_annotator();
    setup_video_info();
}


function setup_annotator() {
    var options = {
        optionsAnnotator: {
            permissions: { },
            store: {
                prefix: 'http://danielcebrian.com/annotations/api',
                annotationData: {uri:'http://danielcebrian.com/annotations/demo.html'},
                loadFromSearch:{
                    limit:10000,
                    uri: 'http://danielcebrian.com/annotations/demo.html',
                }
            },
            richText: {

                tinymce:{
                    selector: "textarea",
                    plugins: "",
                    menubar: false,
                    toolbar_items_size: 'small',
                    extended_valid_elements : "iframe[src|frameborder|style|scrolling|class|width|height|name|align|id]",
                    toolbar: "",
                }

/*
                tinymce:{
                    selector: "textarea",
                    plugins: "",
                    menubar: false,
                    toolbar_items_size: 'small',
                    extended_valid_elements : "iframe[src|frameborder|style|scrolling|class|width|height|name|align|id]",
                    toolbar: "",
                }*/
            },
            share: {}, 
            annotator: {}, 
        },
        optionsVideoJS: {techOrder: ["html5","flash","youtube"]},
        optionsRS: {},
        optionsOVA: {posBigNew:'none'/*,NumAnnotations:20*/},
    }
    ova = new OpenVideoAnnotation.Annotator($('#airlock'),options);
}

function setup_video_info() {
  var yt_url = sprintf("https://www.youtube.com/watch?v=%s", params.videoid);
  var num_annotations = Object.keys(params.annotations).length;
  var html_str = "<div id=\"video-info\"><h3> Video ID: %s </h3>" + 
                 "<span style=\"margin-left: 5px;\">Url: %s </br></span>" +
                 "<span style=\"margin-left: 5px;\">Number of annotations: %d </br></span>";
  html_str = sprintf(html_str, params.videoid, yt_url, num_annotations);
  var save_btn = "<button id=\"save-btn\" type=\"button\" class=\"btn btn-info btn-save\"><span class=\"glyphicon glyphicon-floppy-save\"></span> Save quality assessment </button>";
  html_str = html_str + save_btn + "</div>";
  $("#video-info-div").html(html_str);

  $("#save-btn").click(function(){
    var cnt = 0;
    var ann_keys = Object.keys(params.annotations);
    var num_annotations = ann_keys.length;
    for (var idx=0; idx<num_annotations; idx++) {
      if (params.annotations[ann_keys[idx]]["quality"]) {
        cnt++
      }
    }
    if (cnt==num_annotations) {
      params.status = "finished";
      save_results();
      var url = sprintf("http://yamdrok.stanford.edu/videoannotation/index.html?userId=%s", params.userid)
      window.location.replace(url)
    }
    else {
      bootbox.alert("<h3>Please verify all the temporal annotations.</h3>")
    }
  });
}

function save_results() {
    var videoid = params.videoid;
    var userid = params.userid;
    var results = JSON.stringify(params.annotations);
    $.ajax({
        url: "http://yamdrok.stanford.edu/videoannotation_server/server.py",
        data: {action: "save_quality_results", videoid:videoid, results: results, userid: userid},
        type: "POST",
        async: false,
        dataType: "json",
        success: function(json_data) {
            console.log(json_data);
        }
    });
}

