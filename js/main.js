var ANNOTATIONS = new Array();
$(function() {

    window.onbeforeunload = function() {
        var videoid = params.videoid;
        if (params.status == "finished") {
            return null;
        }
        $.ajax({
            url: "http://yamdrok.stanford.edu/videoannotation_server/server.py",
            data: {action: "enable_video", videoid: videoid},
            type: "POST",
            dataType: "json",
            async: false,
            success: function(json_data) {
                console.log("Leaving..."); 
            }
        });
        return null;
    }

    Pace.on("done", function(){
        $(".cover").fadeOut(2000);
    });

    boot();
    function boot() {
        // Get parameters from url.

        $("#btn-instructions").click(function(){
           /*
           bootbox.alert("<iframe src=\"http://yamdrok.stanford.edu/videoannotation/guidelines/\"></iframe>") 
        */

        bootbox.dialog({ 
          size: 'large',
          message: "<iframe height=\"480px\" width=\"100%\" src=\"http://yamdrok.stanford.edu/videoannotation/guidelines/\"></iframe>", 
          callback: function(){ /* your callback code */ }
        });

        });
        console.log("Botting...");
        params = get_parameters();
        params.status = "unfinished"
        console.log(sprintf("User: %s", params.userid));
        console.log(sprintf("Video: %s", params.videoid));
        if (!params.userid) {
           bootbox.prompt("What is your name?", function(result) {                
           if (result === null) {
               params.userid = "Unknown"                                             
           }
           else {
               params.userid = result;
           }
        });      
        }
        if (!params.videoid) {
            get_random_video();
        }
        else {
            get_video_info(params.videoid);
        }
    }

/*
    function get_random_video() {
        var video_id = null;
        var url = "data/video_status.json";
        $.getJSON(url, function(json){
            var vids = Object.keys(json);
            index = getRandomInt(0, vids.length);
            params.videoid = vids[index];
            get_video_info(params.videoid);
        });
    }*/

    function get_random_video() {
        $.ajax({
            url: "http://yamdrok.stanford.edu/videoannotation_server/server.py",
            data: {action: "get_random_video", userid: params.userid},
            type: "POST",
            dataType: "json",
            success: function(json_data) {
                params.videoid = json_data;
                get_video_info(params.videoid);
            }
        });
    }

    function get_video_info(videoid) {
        $.ajax({
            url: "http://yamdrok.stanford.edu/videoannotation_server/server.py",
            data: {action: "retrieve_video_info", videoid: videoid},
            type: "POST",
            dataType: "json",
            success: function(json_data) {
                ANNOTATIONS = json_data;
                params.annotations = new Object();
                for (var idx=0; idx<ANNOTATIONS.length; idx++) {
                    var ann = ANNOTATIONS[idx];
                    params.annotations[ann.annotation_id] = new Object();
                    params.annotations[ann.annotation_id]["start"] = ann.rangeTime.start;
                    params.annotations[ann.annotation_id]["end"] = ann.rangeTime.end;
                    params.annotations[ann.annotation_id]["label"] = ann.text;
                    params.annotations[ann.annotation_id]["quality"] = "";
                }
                boot_ui();
            }
        });
    }


    function get_parameters() {
        var retval = new Object();
        if (window.location.href.indexOf("?") == -1) {
            retval.userid = null;
            retval.videoid = null;
            return retval;
        }
        var  params = window.location.href.split("?")[1].split("&");
        for (var i in params) {
            var sp = params[i].split("=");
            if (sp.length <= 1) {
                continue;
            }
            var result = sp[1].split("#")[0];
            if (sp[0] == "userId")
            {
                retval.userid = result;
            }
            else if (sp[0] == "videoId")
            {
                retval.videoid = result;
            }
            else
            {
                retval[sp[0]] = result;
            }
        }
        return retval;
    }


function get_annotation(videoid, userid) {
    $.ajax({
        url: "http://yamdrok.stanford.edu/videoannotation_server/server.py",
        data: {action: "retrieve_video_info", videoid: videoid, userid: userid},
        type: "POST",
        dataType: "json",
        success: function(json_data) {
            console.log(json_data);
        }
    });
}

});
