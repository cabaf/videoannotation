function print_annotations(annotation) {
    var activity_name = annotation.text;
    var start = annotation.rangeTime.start;
    var end = annotation.rangeTime.end;
    var yt_url = sprintf("<a target=\"_blank\" href=\"https://www.youtube.com/v/%s?start=%0.1f&end=%0.1f&autoplay=1\">" +
                         " (See on YouTube) </a>", params.videoid, start, end);
    var base_html = "<div id=\"annotation-info\"> <h3> Activity: %s </h3>" + 
                    "<span style=\"margin-left: 5px;\">Start: %0.3f --- </span>" +
                    "<span style=\"margin-left: 5px;\">End: %0.3f</span>" +
                    "<span style=\"margin-left: 5px;\">%s</span>";
    var html_str = sprintf(base_html, activity_name, start, end, yt_url);
    var button_str = "<div id=\"form-%s\" class=\"btn-group spaced-div\" data-toggle=\"buttons\">" +
                     "<label id=\"btn-Good\" type=\"button\" class=\"btn btn-success\"><input type=\"radio\" name=\"answer-%s\" value=\"Good\">Good</label>" +
                     "<label id=\"btn-Fix\" type=\"button\" class=\"btn btn-warning\"><input type=\"radio\" name=\"answer-%s\" value=\"Fix\">Fix</label>" +
                     "<label id=\"btn-Bad\" type=\"button\" class=\"btn btn-danger\"><input type=\"radio\" name=\"answer-%s\" value=\"Bad\">Bad</label>" +
                     "</div>";
    button_str = sprintf(button_str, annotation.annotation_id, annotation.annotation_id, annotation.annotation_id, annotation.annotation_id);
    html_str = html_str + button_str + "</div>";
    $("#annotation-div").html(html_str);
    var radio_str = sprintf("input[name=answer-%s]", annotation.annotation_id);
    $(radio_str).change( function() {
        var ann_id = this.name.split("-")[1];
        params.annotations[ann_id]["quality"] = $(this).val();
        console.log(params.annotations[ann_id]["quality"]);
    });

    if (params.annotations[annotation.annotation_id]["quality"]) {
       var rb_str = "#btn-" + params.annotations[annotation.annotation_id]["quality"];
       $(rb_str).addClass('active');
    }
}


function hide_annotations(){
    $("#annotation-info").remove();
}
